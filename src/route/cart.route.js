const express = require('express')
const router = express.Router()
const { CartAddItem, CartRemoveItem, GetCart, TestPromiseVsAwait } = require('../controller/cart/cart.controller')
const { requireSignin } = require('../../service-share/middle-ware/token.middle-ware')
const { ValidateAddCart } = require('../validate/cart.validator')
const { IsRequestValidated } = require('../../service-share/validator/common-validator')

router.get('/', requireSignin, GetCart)
router.post('/', requireSignin, ValidateAddCart, IsRequestValidated, CartAddItem)
router.patch('/', requireSignin, CartRemoveItem)

router.get('/test', TestPromiseVsAwait)

module.exports = router