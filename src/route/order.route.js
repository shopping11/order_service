const express = require('express')
const router = express.Router()
const { CreateOrder, GetOrder, GetOrderUser, PatchOrderStateSuccess, PatchRequestCancleOrder, CancleOrder } = require('../controller/order/order.controller')
const { requireSignin } = require('../../service-share/middle-ware/token.middle-ware')
const { ValidateOrderQuery, ValidateOrderHistoryQuery, ValidateOrderSatePendingToSuccess, ValidateRequestCancleOrder } = require('../validate/order.validator')
const { IsRequestValidated } = require('../../service-share/validator/common-validator')

router.post('/', requireSignin, CreateOrder)
router.get('/', requireSignin, ValidateOrderQuery, IsRequestValidated, GetOrder)
router.post('/order-history',requireSignin, ValidateOrderHistoryQuery, IsRequestValidated, GetOrderUser)
router.patch('/update-state',requireSignin, ValidateOrderSatePendingToSuccess, IsRequestValidated, PatchOrderStateSuccess )
router.patch('/cancle-order-request', requireSignin, ValidateRequestCancleOrder, IsRequestValidated, PatchRequestCancleOrder)
router.patch('/cancle-order-accept',requireSignin, CancleOrder)

module.exports = router