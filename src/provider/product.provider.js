const axios = require('axios')


exports.post = async (endpoint, token, data) => {
    const url = `${process.env.PRODUCT_SERVICE_BASEURL}/${endpoint}`
    const response = await axios.post(url, data,
        {
            headers: {
                Authorization:  token
            },
        })
    
    return response.data.data 
}

exports.patch = async (endpoint, token, data) => {
    const url = `${process.env.PRODUCT_SERVICE_BASEURL}/${endpoint}`
    const response = await axios.patch(url, data,
        {
            headers: {
                Authorization:  token
            },
        })
    
    return response.data.data 
}

exports.get = async ( endpoint ) => { 
    const url = `${process.env.PRODUCT_SERVICE_BASEURL}/${endpoint}`
    console.log(url)
    const response = await axios.get(url)
    
    return response.data.data 
}