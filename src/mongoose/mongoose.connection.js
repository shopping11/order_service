const mongoose = require("mongoose")

module.exports = mongooseConnection = () => {
    mongoose.connect( `${process.env.MONGODB_URI}`,
    {
      useCreateIndex: true,
      useUnifiedTopology: true,
      useFindAndModify: true,
      useNewUrlParser: true,
      
    }
  )
  .then(() => {
    console.log("Database connected")
  }).catch((err) => {
      console.log(err)
  })
  
  mongoose.set('toObject', {
    transform(doc, { _id, __v, id, ...filter }, options) {
      const filterId = _id || id

      if (!filterId) {
        return filter
      }

      filter.id = typeof filterId === 'object'
        ? String(filterId)
        : filterId

      return filter
    }
  })

}