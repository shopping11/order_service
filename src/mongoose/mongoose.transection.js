const mongoose = require('mongoose');

const withTransaction = async func => {
  const session = await mongoose.startSession();

  session.startTransaction();

  try {
    result = await func(session);
    await session.commitTransaction();
    session.endSession();
  } catch (error) {
    await session.abortTransaction();
    session.endSession();
    throw error;
  }

  return result
};




module.exports = withTransaction;