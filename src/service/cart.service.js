const CartModel = require('../model/cart/cart.schema')

exports.CreateCart = async (req, payload) => { 
    const doc = new CartModel(payload)
    doc.setAuthor(req.user)
    await doc.save()

    return doc.toObject()
}

exports.FindCart = async (req) => { 
    const doc = await CartModel.findOne({ "createdBy.username": req.user.username })
    
    return doc
}

exports.UpdateCart = async (document, req, payload) => { 
    const options = {
        _requestUser: req.user
    }


    return document.set(payload).save(options)
}

exports.GetCart = async (req) => { 
    const doc = await CartModel.findOne({ "createdBy.username": req.user.username })
    if (!doc) { 
        return []
    }
    return doc.toObject()
}

exports.TestAwait = async () => { 
    const doc = await CartModel.find()

    return doc
}

exports.ClearCart = async (req) => { 
    await CartModel.findOneAndDelete({ "createdBy.username": req.user.username })

    return []
  }