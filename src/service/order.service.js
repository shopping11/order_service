const OrderModel = require("../model/order/order.schema");

exports.CreateOrder = async (req,session) => {
  const doc = new OrderModel(req.body);
  doc.setAuthor(req.user)
  await doc.save({session});

  return doc.toObject();
};

exports.GetPaginateOrder = async (req) => { 
  const query = {
    orderStatus: req.query.state,
    status: 'active',
    $or: [
      { 'code': { $regex: req.query.search, $options: "i" } },
      { 'orderItems.name': { $regex: req.query.search, $options: "i" }}
    ],
  }
  const options = {
    page: req.query.page,
    limit: req.query.limit,
    sort: { createdAt: req.query.orderBy },
  }
  const { docs, ...filter } = await OrderModel.paginate(query, options)
  
  return {docs: docs.map(doc => doc.toObject()),filter}
} 

exports.GetPaginateOrderByUser = async (req) => { 
  const query = {
    orderStatus: req.query.state,
    status: 'active',
    "createdBy.username": req.user.username
  }
  const options = {
    page: req.body.page,
    limit: 10,
    sort: { createdAt: 'desc' },
  }
  const { docs, ...filter } = await OrderModel.paginate(query, options)
  
  return {docs: docs.map(doc => doc.toObject()),filter}
}

exports.FindOrderById = async (req) => { 
  const doc = await OrderModel.findOne({ _id: req.body.id })
    
  return doc
} 

exports.UpdateOrderState = async (document, req, payload) => { 
  const options = {
    _requestUser: req.user
}

return document.set(payload).save(options)
}

exports.UpdateOrderStateTransection = async (document, req, payload, session) => { 
  const options = {
    _requestUser: req.user,
    session: session
}

return document.set(payload).save(options)
}