const { check } = require('express-validator')

exports.ValidateAddCart = [
    check('id')
        .notEmpty()
        .withMessage('กรุณารุะบุ'),
    check('code')
        .notEmpty()
        .withMessage('กรุณารุะบุ'),
        check('name')
        .notEmpty()
        .withMessage('กรุณารุะบุ'),
    check('price')
        .notEmpty()
        .withMessage('กรุณารุะบุ')
        .bail()
        .isNumeric()
        .withMessage('กรุณารุะบุให้ถูกต้อง'),
    check('quantity')
        .notEmpty()
        .withMessage('กรุณารุะบุ')
        .bail()
        .isNumeric()
        .withMessage('กรุณารุะบุให้ถูกต้อง'),
    check('bookingDates')
        .notEmpty()
        .withMessage('กรุณารุะบุ'),
    check('productPickType')
        .optional()
]