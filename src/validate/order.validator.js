const { check, query } = require('express-validator')

exports.ValidateOrderQuery = [
    query('limit')
        .notEmpty()
        .default(10),
    query('page')
        .notEmpty()
        .default(1),
    query('orderBy')
        .optional()
        .default('desc'),
    query('search')
        .optional(),
    query('state')
        .notEmpty()
        .default('pending')     
]

exports.ValidateOrderHistoryQuery = [
    query('state')
        .notEmpty()
        .default(10)  
]

exports.ValidateOrderSatePendingToSuccess = [
    check('id')
        .notEmpty(),
    check('receiveMoney')
        .notEmpty()
        .withMessage('กรุณารุะบุ')
        .bail()
        .isNumeric()
]

exports.ValidateRequestCancleOrder = [
    check('id')
        .notEmpty(),
]