const mongoose = require('mongoose')
const { AuthorStampSchema } = require('../../../service-share/model/author-stamp.schema')
const { OrderItem } = require('../order/orderItem.schema')
const { globalMethodPlugin } = require('../../../service-share/plugin/global.method')
const authorStampPlugin = require('../../../service-share/plugin/author-stamp.plugin')

const CartSchena = new mongoose.Schema({
    cartItems: {
        type: [OrderItem],
        default: []
    },
    totalPrice: {
        type: Number,
        default: 0
    },
    totalItem: {
        type: Number,
        default:0
    },
    createdBy: {
        type: AuthorStampSchema,
        default: {}
    },
    updatedBy: {
        type: AuthorStampSchema,
        default: {}
    }
}, { timestamps: true })

CartSchena.plugin(globalMethodPlugin)
CartSchena.plugin(authorStampPlugin)

module.exports = mongoose.model("Cart", CartSchena)