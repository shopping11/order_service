exports.Status = {
    ACTIVE: "active",
	INACTIVE: "inactive"
}

exports.OrderStatus = {
    PENDING: "pending",
    SUCCESS: "success",
    CANCLE_PENDING: "cancle_pending",
    CANCLE: "cancle"
}