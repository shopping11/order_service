const mongoose = require("mongoose")
const AuthorStampSchema = require('../../../service-share/model/author-stamp.schema')
const { OrderItem } = require('./orderItem.schema')
const codeGeneratePlugin = require('../../../service-share/plugin/codeGenerate.plugin')
const authorStampPlugin = require('../../../service-share/plugin/author-stamp.plugin')
const { globalMethodPlugin } = require('../../../service-share/plugin/global.method')
const { Status, OrderStatus } = require('./order.enum')
const mongoosePaginate = require('mongoose-paginate-v2')
const { ErrorHandler } = require("../../../service-share/middle-ware/response.middle-ware")

const OrderSchema = new mongoose.Schema({
    code: {
        type: String
    },
    orderItems: {
        type: [ OrderItem ],
        default: []
    },
    totalPrice: {
        type: Number,
        default: 0
    },
    totalItem: {
        type: Number,
        default:0
    },
    receiveMoney: {
        type: Number,
        default: 0
    },
    changeMoney: {
        type: Number,
        default: 0
    },
    createdBy: {
        type: AuthorStampSchema,
        default: {}
    },
    status: {
        type: String,
        enum: [Status.ACTIVE, Status.INACTIVE],
        default: Status.ACTIVE
    },
    orderStatus: {
        type: String,
        enum: [OrderStatus.PENDING, OrderStatus.SUCCESS, OrderStatus.CANCLE_PENDING, OrderStatus.CANCLE],
        default: OrderStatus.PENDING
    },
    updatedBy: {
        type: AuthorStampSchema,
        default: {}
    }
}, { timestamps: true })


OrderSchema.methods = {
    validateState: function (state) { 
        console.log(this.orderStatus , state)
        if (this.orderStatus !== state) {
            throw new ErrorHandler(400, "incorrect state")
          } 
    }
}

OrderSchema.plugin(globalMethodPlugin)
OrderSchema.plugin(authorStampPlugin)
OrderSchema.plugin(mongoosePaginate)
OrderSchema.plugin(codeGeneratePlugin, 'OD')



module.exports = mongoose.model("Order", OrderSchema)
