const mongoose = require('mongoose')
const { PictureSchema } = require('./picture.schema')

const ProductPickType = new mongoose.Schema({
    mainType: {
        type: String
    },
    subType: {
        type: String
    }
}, { _id: false })

exports.OrderItem = mongoose.Schema({
    id: {
        type: mongoose.SchemaTypes.ObjectId
    },
    code: {
        type: String
    },
    name: {
        type: String
    },
    price: {
        type: Number
    },
    picture: {
        type: PictureSchema,
        default: {}
    },
    quantity: {
        type: Number
    },
    productPickType: {
        type: ProductPickType
    },
    bookingDates: {
        type: [],
        default: []
    }

    
}, {_id: false})