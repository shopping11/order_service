const { asyncHandler, ErrorHandler, AppResponseError } = require("../../../service-share/middle-ware/response.middle-ware")
const CartService = require('../../service/cart.service')
const ProductProvider = require('../../provider/product.provider')
const { APP_BAD_REQUEST } = require("../../../service-share/const/bad-request.const")

exports.GetCart = asyncHandler(async (req, res) => { 
    
    return await CartService.GetCart(req)
})

const BuildPickType = (productPickType) => { 
    const buildQtyPickType = []
    productPickType.productTypes.map((productType) => { 
        if (productType.subType) {
                productType.subType.productSubTypes.map((productSubType) => {
                    buildQtyPickType.push({
                        mainType: productType.nameType,
                        subType: productSubType.nameType,
                        quantity: productSubType.quantity
                    })
                 })
        } else { 
            buildQtyPickType.push({
                mainType: productType.nameType,
                quantity: productType.quantity
            })
        }

    })
    
    return buildQtyPickType
}

const CheckAllowToAddCart = (groupDates, buildQtyPickType) => { 
    if (buildQtyPickType.length > 0) { 
        groupDates.forEach((groupDate) => { 
            if (buildQtyPickType.some((pickType) => 
            groupDate.productPickType?.mainType + '-' + groupDate.productPickType?.subType === pickType?.mainType + '-' + pickType?.subType &&
            groupDate.quantity > pickType.quantity
            )) { 
               throw new AppResponseError(400, APP_BAD_REQUEST.CART_ITEM_NOT_ENOUGHT)
            }
        })
    }
    groupDates.forEach((groupDate) => { 
        if (groupDate.quantity > buildQtyPickType) { 
            throw new AppResponseError(400, APP_BAD_REQUEST.CART_ITEM_NOT_ENOUGHT)
        }
    })
}

const ChckAddCart = (cart, product) => {
    const groupDates = []
    
    cart.bookingDates.forEach((bookingDate) => { 
        if (product.groupBoookingDates.some((groupBoookingDate) =>  new Date(bookingDate) + '-' + cart.productPickType?.mainType + '-' + cart.productPickType?.subType ===
        new Date(groupBoookingDate.date) + '-' + groupBoookingDate.productPickType?.mainType + '-' +
            groupBoookingDate.productPickType?.subType )) { 
            
            const findData = product.groupBoookingDates.find((groupBoookingDate) => new Date(bookingDate) + '-' + cart.productPickType?.mainType + '-' + cart.productPickType?.subType ===
            new Date(groupBoookingDate.date) + '-' + groupBoookingDate.productPickType?.mainType + '-' +
            groupBoookingDate.productPickType?.subType)


            groupDates.push({...findData, quantity: findData.quantity + cart.quantity})
            
            return 
        }

        groupDates.push({ date: new Date(bookingDate), quantity: cart.quantity, productPickType: cart.productPickType})
    })


    if (product.productPickType?.productTypes?.length > 0 ) {
        buildQtyPickType = BuildPickType(product.productPickType)
    } else { 
        buildQtyPickType = product.quantity
    }

    

    return CheckAllowToAddCart(groupDates ,buildQtyPickType)
}

exports.CartAddItem = asyncHandler(async (req, res) => { 
    const doc = await CartService.FindCart(req)
    const product = await ProductProvider.get(`product/group-booking-date/${req.body.id}`)
    const item = req.body

    if (doc) {
        const findProductinCartExist = doc.cartItems.findIndex((cartItem) => item.name + '-' + item.productPickType?.mainType + '-' + item.productPickType?.subType === 
        cartItem.name + '-' + cartItem.productPickType?.mainType + '-' + cartItem.productPickType?.subType)
        
        if (findProductinCartExist < 0 ) { 
            ChckAddCart(item, product)
            doc.cartItems = [...doc.cartItems, item]

            const groupCart = doc.cartItems.reduce((acc, curr) => {
                acc.totalItems += curr.quantity
                acc.totalPrice += (curr.price*curr.quantity) 
              
                return acc
            }, { totalItems: 0, totalPrice: 0 })

            doc.totalPrice = groupCart.totalPrice
            doc.totalItem = groupCart.totalItems

            return await CartService.UpdateCart(doc, req , doc)
        }
        
        doc.cartItems[findProductinCartExist] = { ...item, quantity: doc.cartItems[findProductinCartExist].quantity + item.quantity }
        ChckAddCart(doc.cartItems[findProductinCartExist], product)

        const groupCart = doc.cartItems.reduce((acc, curr) => {
            acc.totalItems += curr.quantity
            acc.totalPrice += (curr.price*curr.quantity) 
          
            return acc
        }, { totalItems: 0, totalPrice: 0 })

        doc.totalPrice = groupCart.totalPrice
        doc.totalItem = groupCart.totalItems
        
        return await CartService.UpdateCart(doc, req, doc)
        
    }    

    ChckAddCart(req.body, product)

    const payload = {
        totalPrice: req.body.price*req.body.quantity,
        totalItem: req.body.quantity,
        cartItems: req.body
    }

    return await CartService.CreateCart(req, payload)
})

exports.CartRemoveItem = asyncHandler(async (req, res) => { 
    const doc = await CartService.FindCart(req)
    const item = req.body
        
    doc.cartItems = doc.cartItems.filter((cartItem) => cartItem.id + '-' + cartItem.productPickType?.mainType + '-' + cartItem.productPickType?.subType !== item.id + '-' + item?.mainType + '-' + item?.subType)
    doc.totalPrice = doc.cartItems.reduce((acc, curr) => acc += curr.price*curr.quantity, 0)
    doc.totalItem = doc.cartItems.reduce((acc, curr) => acc += curr.quantity, 0)
    
    return await CartService.UpdateCart(doc, req ,doc)
})

exports.TestPromiseVsAwait = asyncHandler(async (req, res) => { 
    // const doc = await CartService.TestAwait()
    // const doc2 = await CartService.TestAwait()
    const doc =  CartService.TestAwait()
    const doc2 =  CartService.TestAwait()
    Promise.all([doc, doc2])
    return doc, doc2
})