const { asyncHandler, ErrorHandler, AppResponseError } = require("../../../service-share/middle-ware/response.middle-ware")
const OrderService = require('../../service/order.service')
const ProductProvider = require('../../provider/product.provider')
const withTransaction = require('../../mongoose/mongoose.transection')
const CartService = require('../../service/cart.service')
const { OrderStatus } = require('../../model/order/order.enum')

exports.CreateOrder = asyncHandler(async (req, res) => {
    const { orderItems } = req.body
    req.body.totalPrice = orderItems.reduce((acc, curr) => acc += curr.price*curr.quantity, 0)
    req.body.totalItem = orderItems.reduce((acc, curr) => acc += curr.quantity,0)
    
    return withTransaction(async (ClientSession) => { 
    const order = await OrderService.CreateOrder(req,ClientSession);


    const buildOrderItems = orderItems.map((orderItem) => ({
        id: orderItem.id,
        code: orderItem.code,
        quantity: orderItem.quantity,
        bookingDates: orderItem.bookingDates,
        productPickType: orderItem.productPickType

    }))
        
        const payload = {
            id: order.id,
            code: order.code,
            orderItems: buildOrderItems
        }

        await ProductProvider.patch('product/order/booking', req.headers.authorization, payload)
        
        const doc = await CartService.ClearCart(req)

        return  doc
    })

})

exports.GetOrder = asyncHandler(async (req, res) => {

    return OrderService.GetPaginateOrder(req)
 
})

exports.GetOrderUser = asyncHandler(async(req, res) => {

    return OrderService.GetPaginateOrderByUser(req)
})

exports.PatchOrderStateSuccess = asyncHandler(async (req, res) => { 
    const orderDoc = await OrderService.FindOrderById(req)

    orderDoc.validateState(OrderStatus.PENDING)
    
    if (req.body.receiveMoney < orderDoc.totalPrice) { 
        throw new ErrorHandler(400, "Error input money less than total price")
    }
    
    orderDoc.receiveMoney = req.body.receiveMoney
    orderDoc.changeMoney =  req.body.receiveMoney - orderDoc.totalPrice
    orderDoc.orderStatus = OrderStatus.SUCCESS

    return OrderService.UpdateOrderState(orderDoc,req,orderDoc)
})

exports.PatchRequestCancleOrder = asyncHandler(async (req, res) => { 
    const orderDoc = await OrderService.FindOrderById(req)
    orderDoc.validateState(OrderStatus.PENDING)
    orderDoc.orderStatus = OrderStatus.CANCLE_PENDING
    
    return OrderService.UpdateOrderState(orderDoc,req,orderDoc)
}) 

exports.CancleOrder = asyncHandler(async (req, res) => { 
    const orderDoc = await OrderService.FindOrderById(req)
    orderDoc.validateState(OrderStatus.CANCLE_PENDING)

    return withTransaction(async (ClientSession) => { 
        const payload = {
            id: orderDoc.id,
            orderItems: orderDoc.orderItems
        }
        orderDoc.orderStatus = OrderStatus.CANCLE

        await OrderService.UpdateOrderStateTransection(orderDoc,req,orderDoc, ClientSession)
        
        return  await ProductProvider.patch('delete/booking-product', req.headers.authorization, payload)
    })
})

